import glob
import json
import os
import subprocess

from .exceptions import ToolNotFound

try:
    from shutil import which
except ImportError:
    # python2.7 compatibility

    def which(tool):
        which = subprocess.Popen(["which", tool], stdout=subprocess.PIPE)
        stdout, stderr = which.communicate()
        if which.returncode:
            return None

        bin_path = stdout.strip().decode("utf-8")
        return bin_path


def locate_binary(possible_names):
    for name in possible_names:
        binary = which(name)
        if binary:
            return binary
    return None


def check_tool(name):
    binary = which(name)
    if not binary:
        raise ToolNotFound("Cannot locate a tool: '{}'".format(name))


def get_immediate_subdirectories(dir):
    return [os.path.join(dir, child) for child in os.listdir(
        dir) if os.path.isdir(os.path.join(dir, child))]


def mkdir(dir, parents=False):
    cmd = ["mkdir"]
    if parents:
        cmd.append("-p")
    cmd.append(dir)
    subprocess.check_call(cmd)


def glob_expand(patterns):
    files = set()
    for pattern in patterns:
        files.update(glob.glob(pattern))
    return list(files)


def filter_out(items, blacklist):
    return [item for item in items if item not in blacklist]


def load_json(path):
    if not os.path.exists(path):
        raise RuntimeError("File not found: {}".format(path))

    with open(path, "r") as f:
        return json.load(f)
