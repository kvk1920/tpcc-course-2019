import sys

from . import highlight


class Echo(object):
    def __init__(self, ctx=None):
        self.ctx = ctx

    def wrap(self, ctx):
        return Echo(ctx)

    def echo(self, line):
        sys.stdout.write(self._add_context(line) + '\n')

    def write(self, text):
        sys.stdout.write(self._add_context(text) + '\n')

    def separator_line(self):
        sys.stdout.write(('-' * 80) + '\n')

    def blank_line(self):
        sys.stdout.write('\n')

    def success(self, text):
        sys.stderr.write(highlight.success(self._add_context(text)) + '\n')

    def done(self):
        self.blank_line()
        self.success("Done")

    def error(self, text):
        sys.stderr.write(highlight.error(self._add_context(text)) + '\n')

    def _add_context(self, text):
        if self.ctx:
            return "[{}] {}".format(self.ctx, text)
        else:
            return text


echo = Echo()
