#include "sleepsort.hpp"

#include <twist/stdlike/thread.hpp>

#include <twist/support/compiler.hpp>

#include <chrono>

// Acts like std::thread
using twist::thread;

using twist::this_thread::sleep_for;

namespace solutions {

void SleepSort(std::vector<int>& ints) {
  // Use 'thread' class to launch/join threads

  // Use 'sleep_for' function to sleep
  // E.g. sleep_for(std::chrono::microseconds(42));

  // Your code goes here
  UNUSED(ints);
}

}  // namespace solutions
